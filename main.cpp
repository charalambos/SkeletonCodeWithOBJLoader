#include <iostream>
#include <GL/glew.h>            ///Include GLEW header files
#include <GLFW/glfw3.h>         ///Include GLFW header files
#include <glm/glm.hpp>          ///Include GLM header files
#include <glm/gtc/constants.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <vector>
#include <fstream>
#include <iostream>


#include "OBJ.h"
#include "OBJParser.h"
#include "OBJParser.lex.h"
#include "OBJParser.yacc.hpp"

#define VIEWER_WIDTH    800
#define VIEWER_HEIGHT   800

#define FOVY    M_PI*60.0/180.0
#define NEAR    0.01
#define FAR     10000.0

///The handle to the window
GLFWwindow *window_handle = 0x00;
///IDs of shaders
GLuint vertex_shader_id = 0;
GLuint fragment_shader_id = 0;
GLuint shader_program_id = 0;
///ID for the VAO, VBO, EBO
GLuint vao = 0;
GLuint vbo = 0;
GLuint ebo = 0;
///ctm location
GLuint ctm_id = 0;
///the perspective matrix
glm::mat4 persp_matrix = glm::mat4(1.0);
///the eye, lookat, up
glm::vec3 eye = glm::vec3(0.0,0.0,10.0);
glm::vec3 dir = glm::vec3(0.0,0.0,-1.0);
glm::vec3 up = glm::vec3(0.0,1.0,0.0);
///the view matrix
glm::mat4 view_matrix = glm::lookAt(eye,eye+dir,up);
///the model matrix
glm::mat4 model_matrix = glm::mat4(1.0);
///the ctm matrix
glm::mat4 ctm = glm::mat4(1.0);

///the vertices and normals of the object
std::vector<glm::vec3> vertices, normals;
std::vector<unsigned int> vertex_indices;


GLuint loadShaders(std::string vertex_shader_path, std::string fragment_shader_path) {

    // Create the shaders
    vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_shader_path.c_str(), std::ios::in);

    if (VertexShaderStream.is_open()) {
        std::string Line = "";
        while (getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }
    else {
        printf("Impossible to open %s. Are you in the right directory ?\n", vertex_shader_path.c_str());
        getchar();
        exit(-1);
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_shader_path.c_str(), std::ios::in);

    if (FragmentShaderStream.is_open()) {
        std::string Line = "";
        while (getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }
    else {
        printf("Impossible to open %s. Are you in the right directory ?\n", fragment_shader_path.c_str());
        getchar();
        exit(-1);
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_shader_path.c_str());
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(vertex_shader_id, 1, &VertexSourcePointer, 0x00);
    glCompileShader(vertex_shader_id);

    // Check Vertex Shader
    glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(vertex_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
        glGetShaderInfoLog(vertex_shader_id, InfoLogLength, 0x00, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }

    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_shader_path.c_str());
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(fragment_shader_id, 1, &FragmentSourcePointer, 0x00);
    glCompileShader(fragment_shader_id);

    // Check Fragment Shader
    glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(fragment_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
        glGetShaderInfoLog(fragment_shader_id, InfoLogLength, 0x00, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }

    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, vertex_shader_id);
    glAttachShader(ProgramID, fragment_shader_id);

    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if (InfoLogLength > 0) {
        std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, 0x00, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
    return ProgramID;
}


bool initialize()   {

    ///Initialize the GLFW window
    if (!glfwInit()) return false;

    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 5 );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    ///Create the window
    window_handle = glfwCreateWindow(VIEWER_WIDTH, VIEWER_HEIGHT, "COMP371 - ObjectLoading",0x00,0x00);
    if (!window_handle)    {
        std::cout << "ERROR: Failed to create a window." << std::endl;
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window_handle);

    ///Initialize GLEW extension handler
    glewExperimental = GL_TRUE; ///Needed to get the latest version of OpenGL
    glewInit();

    ///Test if all is good
    const GLubyte  *renderer = glGetString(GL_RENDERER);
    std::cout << "Renderer: " << renderer << std::endl;
    const GLubyte  *version = glGetString(GL_VERSION);
    std::cout << "OpenGL version: " << version << std::endl;

    ///Enable the depth test
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);


    return true;
}

bool cleanup()  {
    //Release memory e.g. vao, vbo, etc
    glDeleteProgram(shader_program_id);
    glDeleteShader(vertex_shader_id);
    glDeleteShader(fragment_shader_id);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);

    if (window_handle)     glfwDestroyWindow(window_handle);
    window_handle = 0x00;

    glfwTerminate();

    return true;
}

void windowsizeCallback (GLFWwindow *window_handle, int width, int height)  {
    ///set the viewport to have the same size as the window
    glViewport(0,0,width,height);

    ///set the perspective matrix
    double aspect = double(width)/double(height);
    persp_matrix = glm::perspective(FOVY, aspect, NEAR, FAR);

    return;
}

void loadObjects(const char *file_name)  {
    ///Read in all the contents of the file
    std::ifstream ifs(file_name);
    std::string content( (std::istreambuf_iterator<char>(ifs) ),
                         (std::istreambuf_iterator<char>()    ) );

    //If you want to debug through the code you can pass the data as a string
    //and the parser will parse the string instead of the file like below:
    //YY_BUFFER_STATE buf = OBJParser_scan_string("v 1.0 1.0 1.0\n");

    //Otherwise set the file pointer as the input stream to the parser
    //Set the stream as the input to the parser
    YY_BUFFER_STATE buf = OBJParser_scan_string(content.c_str());

    ///initialize the OBJ
    obj_model = new OBJ();

    ///If you would like to run only the scanner then uncomment the following command
//    OBJParserlex();

    ///This runs the parser
    OBJParserparse();

    ///Clean up
    OBJParser_delete_buffer(buf);
    OBJParserlex_destroy();

    //Show what you got. This is for debugging.
    printf("<------- PRINTING OUT ---------->\n");
    obj_model->Print();
    printf("<------------------------------->\n");

    std::vector<Vector3f> obj_vertices = obj_model->getVertices();
    for (int i=0;i<obj_vertices.size();i++)    {
        Vector3f vertex = obj_vertices[i];
        vertices.push_back(glm::vec3(vertex(0),vertex(1),vertex(2)));
    }

    std::vector<Vector3f> obj_normals = obj_model->getNormals();
    for (int i=0;i<obj_normals.size();i++)    {
        Vector3f normal = obj_normals[i];
        normals.push_back(glm::vec3(normal(0),normal(1),normal(2)));
    }
    bool compute_normals = false;
    if (obj_normals.size() == 0) compute_normals = true;

    std::vector<Face *> faces = obj_model->getFaces();
    for (int i=0;i<faces.size();i++)    {
        Face *face = faces[i];
        for (int j=0;j<face->getNumberOfVertices();j++) {
            vertex_indices.push_back(face->getVertexAt(j));
        }
        if (compute_normals)    {
            Vector3f v1 = obj_vertices[face->getVertexAt(1)] - obj_vertices[face->getVertexAt(0)];
            Vector3f v2 = obj_vertices[face->getVertexAt(2)] - obj_vertices[face->getVertexAt(0)];
            Vector3f normal = v1.cross(v2);
            normal.normalize();
            normals.push_back(glm::vec3(normal(0),normal(1),normal(2)));
        }
    }

    //cleanup (only if you don't plan on using the same data structure to store the geometry and topology information).
    delete obj_model;

    return;
}

int main() {
    std::cout << "2018 Fall - COMP 371 - Assignment #1" << std::endl;
    std::cout << "Loading object and storing data" << std::endl;
    std::cout << "Generate ebo buffer, bind, glDrawElements(.), glDeleteBuffer(.)" << std::endl;

    ///initialize
    initialize();

    ///register callback functions
    glfwSetWindowSizeCallback(window_handle, windowsizeCallback);

    ///call the resize once
    windowsizeCallback(window_handle, VIEWER_WIDTH, VIEWER_HEIGHT);

    ///load the shaders
    shader_program_id = loadShaders("../basic.vs", "../basic.fs");

    ///define geometry
    //loadObjects("../data/Stone_01_Base.OBJ");
    loadObjects("../data/heraclesbust/heraclesbust-exported.obj");
    //loadObjects("../data/pp-exported/pp-exported.obj");

    ///setup the containers
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

#if 1
    ///Either the following two
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);
#else
    /// or the following three. If you uncomment the following three lines make sure you uncomment the equivalent in the shader
    GLuint vertex_position_id = glGetAttribLocation(shader_program_id, "vertex_position");
    glVertexAttribPointer(vertex_position_id, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(vertex_position_id);
#endif

    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertex_indices.size() * sizeof(vertex_indices[0]), &vertex_indices[0], GL_STATIC_DRAW);

    ///get the location of the matrix
    ctm_id = glGetUniformLocation(shader_program_id, "ctm");

    ///start using the shader program
    glUseProgram(shader_program_id);

    std::cout << "Rendering..." << std::endl;
    while (!glfwWindowShouldClose(window_handle))  {
        ///erase the contents every time
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ///compute the ctm
        ctm = persp_matrix * view_matrix * model_matrix;
        ///send the ctm
        glUniformMatrix4fv(ctm_id,1,GL_FALSE,glm::value_ptr(ctm));

        glDrawElements(GL_POINTS, vertex_indices.size(), GL_UNSIGNED_INT, 0);

        ///update other events like input handling
        glfwPollEvents();
        ///swap the buffers
        glfwSwapBuffers(window_handle);
    }

    ///cleanup
    cleanup();

    return 0;
}
