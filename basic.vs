#version 330 core

layout(location = 0) in vec3 vertex_position;
///Uncomment the following line and comment the above if you want to use glGetAttribLocation(...)
//in vec3 vertex_position;

uniform mat4 ctm;

void main() {
    gl_Position = ctm * vec4(vertex_position, 1.0);
}
