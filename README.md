# SkeletonCode

## COMP 371 - Computer Graphics

#### GLFW3, GLM, GLEW, eigen3, flex, bison

This loads an OBJ file and renders the points on screen. 
No input/interaction is implemented other than 
window resizing. 
